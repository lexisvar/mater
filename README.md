*Notice:* This project is no longer active.

# Mater

Mater is a client for the [Free Internet Chess Server](http://www.freechess.org/) (FICS).
The goal is to provide a client that runs on desktop web browsers
without any plugins like Flash or Java.  It is written in JavaScript.

### Status

This is project is still in the experimental stages; it's not yet
usable as a primary interface.

What works:

* Logging in
* Basic playing and chatting
* Saving basic settings locally
* Basic sounds and icons
* Support for non-ASCII characters on FICS through so-called Maciejg format
(compatible with Yafi and Raptor)

What needs to be done, in no particular order:

* make it possible to move pieces by clicking on the source square
  followed by the destination square; this is [chessboardjs issue 34](https://github.com/oakmac/chessboardjs/issues/34).
* support for different window layouts and different screen resolutions
  (the current layout is a mess)
* Jin-style server-side storage of settings (requires authenticating
  to the server)
* saving username and password
* frendly dialog boxes for users at login and after game ends
* a seek graph
* premove
* whispering and kibitzing in games
* translation of interface messages to languages other than English
* dialog boxes so that the console isn't necessary for reading messages,
fingering players, sending tells, finding games, etc.
* examine/bsetup mode
* variants
* smilies
* move history, promotion pieces
* unit tests, integration tests
* PGN import/export
* timestamps on console/chat
* user scripts
* engine analysis

Unlikely to be supported:

* Automatic session logs or logging games locally (these would require access to
the file system, which is not possible)

## Libraries used

* [Ext JS](http://www.sencha.com/products/extjs/) for the interface
* [SockJS](https://github.com/sockjs/sockjs-client) for communication
with the server
* [howler.js](https://github.com/goldfire/howler.js) for sounds
* [ChessBoardJS](https://github.com/ornicar/chessboardjs) for the board
* [chess.js](https://github.com/jhlywa/chess.js) for the legality checking
* [http://gruntjs.com/](Grunt) for running tasks
* [http://yui.github.io/yuicompressor/](YUI Compressor) for minification

For the sources and licenses of the media files, see the README files
in their respective directories. I have tried use only files available
under a free license.


## Testing

A copy for testing with the FatICS test server is hosted at
<https://fatics.org/mater/>.

Testing with FICS is not currently possible unless you set up your
own installation with a web server and run the FICS proxy.

### Supported platforms

The current goal is to support all major browsers: Internet Explorer 7+,
Firefox 3.6+, Safari 4+, Chrome 10+, and Opera 11+. More testing
and bug fixing is needed.

There are no plans to support mobile devices, although some of the code
might be useful in creating a mobile interface.

## Installation and usage

To host a copy of the client yourself, a web server is necessary.
TLS-encrypted connections are supported and recommended. Of course
the connection between the proxy and FICS will still be unencrypted,
because FICS does not support encryption.

[Timeseal 2](http://www.freechess.org/Help/HelpFiles/timeseal.html)
is enabled for all connections by default. Unfortunately the use
of timeseal results in binary data which SockJS cannot handle directly.
Therefore an extra base64 encode/decode step is necessary. This
means that client-to-server communicaton will use about 1.37 times
(according to Wikipedia) more bandwidth than with a traditional FICS
client.

### Proxy

Since the client is not hosted by FICS itself, it is necessary
for connections to be proxied through the web server to FICS.  I've
included a small script for that purpose, written in Python using the
[sockjs-twisted](https://github.com/DesertBus/sockjs-twisted) library.

The necessary Twisted modules must be set up; a public and private key
should be placed in `keys/server.key` and `keys/server.pem`, respectively.
Note that most browsers will refuse to accept such keys by default unless
they are signed by a CA.

## Contributing

Patches are welcome, preferably accompanied by tests.

## Credits

Mater was written by Wil Mahan.

Thanks to:

* Chris Oakman (oakmak) for [chessboard.js](http://www.chessboardjs.com/)
* The authors of other free software FICS clients: eboard, XBoard, Jin,
  Raptor, Thief, PyChess, Yafi, and CClient
* Bronco Wideswinger for hosting
* Gorgonian for advice about finding sounds
* Everyone who plays on FICS

## License
[GPL v3 or later](https://www.gnu.org/licenses/gpl.html); see `COPYING`

## See also

* [http://www.freechess.org](FICS) is the server this interface was written for
* [https://bitbucket.org/wmahan/fatics](FatICS) is a free software equivalent server I'm working on
* [http://lichess.org/](lichess.org) is another server and interface built using free software; I am not affiliated with it other than being a fan of the work
