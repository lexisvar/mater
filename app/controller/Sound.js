/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

/* global console: false, Howl: false, mater:false */
Ext.define('mater.controller.Sound', {
    extend: 'Ext.app.Controller',

    init: function() {
        "use strict";
        this.tell = new Howl({urls: ['resources/snd/tell.ogg']});
        this.bell = new Howl({urls: ['resources/snd/beep.ogg']});
        this.move = new Howl({urls: ['resources/snd/move.ogg']});

        this.application.on({
            sound: this.play,
            scope: this
        });
    },

    play: function(name) {
        "use strict";
        var snd;
        if (!mater.settings.data.sound) {
            return;
        }
        switch(name) {
            case "bell":
                snd = this.bell;
                break;
            case "move":
                snd = this.move;
                break;
            case "tellreceived":
                snd = this.tell;
                break;
            case 'tellsent':
                return;
            default:
                console.log('unknown sound: ' + name);
                return;
        }
        snd.play();
    },
});

/* vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
 */
