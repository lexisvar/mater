/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('mater.view.Settings', {
    extend: 'Ext.window.Window',
    //requires: ['Ext.tab.Panel'],
    alias: 'widget.settings',
    width: '50%',
    height: 340,
    autoShow: false,
    closable: true,
    resizable: false,
    minimizable: false,
    plain: false,
    //layout: 'fit',
    border: true,
    title: 'Settings',
    modal: true,
    closeAction: 'hide',
    //id: 'settings',
    items: [{
        xtype: 'form',
        layout: 'vbox',
        width: '100%',
        height: 320,
        items: [
            {
                xtype: 'tabpanel',
                closable: false,
                width: '100%',
                bodyPadding: 10,
                height: 275,
                items: [
                    {
                        title: 'Playing',
                        items: [
                            {
                                name: 'premove',
                                xtype: 'checkbox',
                                //inputValue: true,
                                boxLabel: 'Enable premove'
                            },
                        ]
                    },
                    {
                        title: 'Sound',
                        items: [
                            {
                                xtype: 'checkbox',
                                name: 'sound',
                                //inputValue: 'true',
                                boxLabel: 'Enable sounds'
                            },
                        ]
                    },
                    {
                        title: 'Console',
                    },
                    {
                        title: 'Advanced',
                        items: [
                            {
                                xtype: 'textarea',
                                id: 'logonScript',
                                name: 'logonScript',
                                fieldLabel: 'Logon script',
                                labelAlign: 'top',
                                height: 210,
                                width: '95%',
                            }
                        ]
                    },
                ]
            },
            {
                xtype: 'container',
                layout: 'hbox',
                items: [
                    { xtype: 'button', text: 'Save settings',
                        margin: 6, id: 'saveSettings' },
                    { xtype: 'button', text: 'Cancel', margin: 6,
                        id: 'cancelSettings'}
                ]
            }
        ]
    }],
});

/* vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
 */
