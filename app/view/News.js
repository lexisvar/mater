/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('mater.view.News', {
	extend: 'Ext.window.Window',
	alias: 'widget.news',

    title: 'News and announcements',
    x: 700,
    y: 530,
    width: '40%',
    height: 210,
    minimizable: true,
    maximizable: true,
    closable: false,
    icon: 'resources/img/news.png',
    bodyStyle: 'background-color: #ffffff;',

    items: [
        {xtype: 'container', itemId: 'newsText', autoScroll: true,
            tpl: '{html}<br />', tplWriteMode: 'append',
            padding: '5px', height: 170, width: '100%',
            cls: 'newsText'}
    ],


    initComponent: function() {
        "use strict";
        this.superclass.initComponent.apply(this);
    }
});

// vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
