/*global module:false*/
/*jshint camelcase: false */
module.exports = function(grunt) {
    "use strict";
    // Project configuration.
    grunt.initConfig({
        // Metadata.
        meta: {
            version: '0.3.0'
        },

        clean: {
            build: ["build"]
        },

        //banner: '/*! mater - v<%= meta.version %> - ' +
        //  '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
        //  '* https://bitbucket.org/wmahan/mater\n' +
        //  '* Copyright (c) <%= grunt.template.today("yyyy") %> ' +
        //  'Wil Mahan; Licensed GPLv3 */\n',
        //// Task configuration.
        concat: {
            /*options: {
           banner: '<%= banner %>',
            stripBanners: true
          },*/
            dist: {
                src: '<%= sencha_dependencies_build %>',
                //src: ['<%= sencha_dependencies_build %>', 'ext-4/src/dom/AbstractElement_style.js', 'ext-4/src/dom/AbstractElement.js'],
                //src: ['ext-4/ext-all.js', '<%= sencha_dependencies_build_app %>'],
                dest: 'build/www/app-all.js'
            }
        },
        /* disabled because it enforces use strict everywhere, breaking ext */
        /*uglify: {
            build: {
                options: {
                    banner: "/* (C) 2014 Wil Mahan. Licensed under GPLv3. https://bitbucket.org/wmahan/mater /",
                    sourceMap: true,
                    beautify: true
                },
                files: {
                    // unfortunately the Ext dependencies seem to be buggy
                    // so we have to use ext-all.js
                    //"build/www/app.min.js": ["<%= sencha_dependencies_build %>"]
                    "build/www/app.min.js": ['ext-4/ext-all.js', "<%= sencha_dependencies_build_app %>"]
                }
            }
        },*/
        min: {
            build: {
                // unfortunately the Ext dependencies seem to be buggy
                // so we have to use ext-all.js
                src: ["<%= sencha_dependencies_build %>"],
                dest: "build/www/app.min.js"
            }
        },
        cssmin: {
            build: {
                src: ["resources/css/*.css", "chessboardjs/css/chessboard.css", "chessboardjs/css/normalize-2.1.2.min.css"],
                dest: "build/www/app.min.css"
            }
        },
        jshint: {
            options: {
                camelcase: true,
                curly: true,
                eqeqeq: true,
                immed: true,
                latedef: true,
                newcap: true,
                noarg: true,
                undef: true,
                //unused: true,
                forin: true,
                unused: 'vars',
                browser: true,
                indent: 4,
                freeze: true,
                nonbsp: true,
                quotmark: false,
                strict: true,
                trailing: true,
                maxerr: 5,
                globals: {Ext: false, mater: true}
            },
            gruntfile: {
                src: 'Gruntfile.js'
            },
            app: {
                src: ['app.js', 'app/**/*.js', 'resources/js/timeseal.js'], //'app-test/*.js']
            },
            tests: {
                src: ['Gruntfile.js', 'app-test/*.js'],
                options: {
                    strict: false,
                    indent: false,
                    globals: {describe: false, it: false, expect: false, Ext: false,
                        mater: true}
                }
            }
        },
        jasmine:  {
            tests: {
                src:['app.js', 'app/**/*.js'],
                options: {
                    specs: ['app-test/app-test.js', 'app-test/login.js'],
                    vendor: 'ext-4/ext-all.js',
                    keepRunner: true
                }
            }
        },
        sencha_dependencies: {
            build: {
                options : {
                    appFile: "app.js",
                    senchaDir: "ext-4",
                    pageToProcess: "index.html"
                }
            }
        },

        copy: {
            build: {
                files: [
                    {expand: true, src: ["ext-4/resources/css/ext-all.css"], dest: "build/www", cwd: "."},
                    {expand: true, src: ["ext-4/resources/ext-theme-classic/**"], dest: "build/www", cwd: "."},
                    {expand: true, src: ["ext-4/examples/ux/statusbar/**"], dest: "build/www", cwd: "."},
                    {expand: true, src: ["chessboardjs/img/**"], dest: "build/www", cwd: "."},
                    {expand: true, src: ["resources/{snd,img}/**"], dest: "build/www", cwd: "."},
                    {expand: true, src: ["index.html"], dest: "build/www", cwd: "."}
                ],
                options: {
                    processContentExclude: ["**/*.{gif,jpg,png,ogg}"],
                    processContent: function (content, filePath) {
                        if (/index.html/.test(filePath)) {
                            //console.log(grunt.template.process("<%= sencha_dependencies_build %>"));
                            // remove all scripts
                            content = content.replace(/<script.*?js"><\/script>\n?/g, "");
                            // remove some css
                            content = content.replace(/<link rel="stylesheet" href="chessboardjs\/css\/.*?\.css" \/>\n?/g, "");
                            content = content.replace(/<link rel="stylesheet" href="resources\/css\/mater.css" \/>\n?/g, "");
                            content = content.replace(/<\/head>/, '    <link rel="stylesheet" href="app.min.css" />\n    <script src="app.min.js"></script>\n</head>');
                        }
                        return content;
                    }
                }
            }
        },

        /*watch: {
          app: {
            files: '<%= jshint.app.src %>',
            tasks: ['jshint:app']
          }
        }*/
        rsync: {
            options: {
                recursive: true,
                args: ["-e 'ssh -p 465'", '--delete']
            },
            build: {
                options: {
                    host: 'faticsweb@fatics.org',
                    src: 'build/www/',
                    dest: "~/public_html/mater"
                }
            }
        }
    });

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-concat'); // tmp
    grunt.loadNpmTasks('grunt-contrib-clean');
    //grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-yui-compressor');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-sencha-dependencies');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-rsync');

    // Default task.
    grunt.registerTask('default', ['jshint', 'clean', 'sencha_dependencies', 'copy', 'cssmin', 'min']);

};

// vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
